=begin

	to_i convierte a entero
	to_f convierte a Float
	to_s convierte a cadena 
	
	print cadena de texto
	puts con salto de linea

=end
# =>  variable global
# =>  It can be used in all class
$global = "variable global"
class Study
	# => initialize
	def initialize()
		# => variable of instance 
		@instancia = "variable de instancia"
		$global = "variable global modificada"
	end
	def varriables()
		# => variable local
		local = "variable local"
		puts instancia
	end
	def operacion()
		valor1 =2
		valor2 =3
		resutado = valor1 + valor2
		print "la suma de 2 + 3 = #{resultado}" 
	end
	def convert_to_int()
		valor1 = "1"
		valor2 = 2
		valor1.to_i
		resultado = valor1 + valor2 
	end
	def concatenar()
		cadena = "hola "
		cadena << "mundo"
	end
	# => conditionals
	# => if
	def conditional_if()
		hora = 10
		if hora < 12
			puts "buenos dias"
		else
			puts "buenas tardes"
		end
	end
	# => Unles contrary if
	def conditional_unless()
		hora = 10
		unless hora > 12
			puts "buenos dias"
		# => no use else
		end
	end
	# => case
	def conditional_case()
		edad = 2
		# => forma 1
		case edad
		when 0..11 then print "a child"
		when 12..17 then print "is a teen"
		when 18..29 then print "is a young"
		when 30..150 then print "is a old"
		else print "Error in the variable"		
		end
		# => forma 2
		resulatado = case edad
		when 0..11 then "a child"
		when 12..17 then "is a teen"
		when 18..29 then "is a young"
		when 30..150 then "is a old"
		else "Error in the variable"		
		end
		print resultado
	end
	# => ciclos
	# => ciclo for
	def ciclo_for
		for i in (0..9)
			if i == 8
				break # => exit ciclo
			elsif i == 6
				next # => next ciclo
			else
				redo # => repeat ciclo
			end
			print i
			print "\n"
		end
	end
	# => each 
	def ciclo_each
		# => each
		(1..10).each { 
			|i| print i
		}
		# => upto ( hacia arriba ) 
		# => 1 -> 10
		1.upto(10){ 
			|i| print i
		}
		# => downto ( hacia abajo ) 
		# => 10 -> 1
		10.downto(1){ 
			|i| print i
		}
		# => times ( star 0 ) 
		# => 0 -> n
		10.times(1){ 
			|i| print i
		}
	end
	def ciclo_while()
		i = 0
		while i < 5 do
			print i
			i += 1
			i = 0
		end
		begin
			print i
			i += 1
		end while i < 5
	end
	# => data entry
	def data_entry()
		puts "dame tu nombre"
		name = gets.chomp
		print "hola " + name
	end
	# => arrays
	def arrays()
		array = [1,2,3,4,5,6,7,8,9,10]
		array.each do |i|
			puts i
		end	
	end
	def hash()
		hash = {'ruby' => 21, 'pagina_web' => 15}
		hash['html5'] = 25
		puts hash['ruby']
		hash.each do |key,value|
			puts "#{key} tiene #{value} videos"
		end
	end
end

objeto = Study.new()
objeto.hash
gets() 