#!/usr/bin/env ruby
#encoding: utf-8
# => require 'gtk3'   
require 'gtk2' 

class Windows

	# => initializer 

	def initialize
		window_show()
	end

	def window_show
		window = Gtk::Window.new
		window_signal_connect(window)
		border_width(10, window)
 		add_button(window, button_show())
 		window.show_all
	end

	# => events

	def window_signal_connect(window)
		window.signal_connect("delete_event") {
		  puts "delete event occurred"
		  #true
		  false
		}

		window.signal_connect("destroy") {
		  puts "destroy event occurred"
		  Gtk.main_quit
		}
	end

	# => style

	# => Windows style

	# => remove all decoration windows

	def remove_all__decoration(window)
		window.decorated = false
	end

	# => window border width

	def border_width(bwidth, window)
		window.border_width = bwidth
	end

	# => buttons

	def button_show
		button = Gtk::Button.new("prueba")
		button.signal_connect("clicked") {
		  puts "sudo apt update"
		}
		return button
	end

	def add_button(window, button)
		window.add(button)
	end

end