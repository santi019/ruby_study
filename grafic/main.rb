#!/usr/bin/env ruby
#encoding: utf-8
# => require 'gtk3'   
require 'gtk2'
require_relative "windows.rb"

class Main < Windows

    def initialize
        super # => call initialize father
        # => super.window()
        Gtk.main
    end

end

program = Main.new()
